const SpecsItem = [
  { title: 'Amp Models', desc: '200'},
  { title: 'Effects Loop', desc: 'Yes'},
  { title: 'Inputs', desc: '2 x 1/4”'},
  { title: 'Outputs', desc: '1 x 1/4”, 2 x XLR, 2 x 1/4”'},
  { title: 'MIDI I/O', desc: 'In/Out/Thru'},
  { title: 'Height', desc: '8.54”'},
  { title: 'Width', desc: '14.88”'},
  { title: 'Depth', desc: '6.81”'},
  { title: 'Weight', desc: '11.73 lbs'},
]

const Specifications = () => (
    <div className="font-medium text-gray-900 leading-5 text-sm border-gray-200 border-b mb-">
      {
        SpecsItem.map((item) => {
          return (
            <div key={item.title} className="flex items-center justify-between p-4 border-t">
              <p className="">{item.title}</p>
              <p className="text-gray-600">{item.desc}</p>
            </div>
          )
        })
      }
    </div>
  )


export default Specifications