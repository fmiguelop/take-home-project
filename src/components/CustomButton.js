import PropTypes from 'prop-types';

const CustomButton = ({ title, desc, price, onClick, selected }) => {
  CustomButton.PropTypes = {
    title: PropTypes.string,
    desc: PropTypes.string,
    price: PropTypes.string,
    onClick: PropTypes.func,
    selected: PropTypes.bool
  }
  return (
    <button
      onClick={onClick}
      className={`${selected ? 'border-gray-700 shadow-outline-gray' : 'border-gray-300' } leading-5 font-medium text-left text-gray-900 text-sm border px-6 py-5 rounded-md flex justify-between items-center w-full my-4  hover:border-gray-400 focus:outline-none focus:shadow-outline-gray  focus:border-gray-700`}>
      <span>
        <p className="whitespace-no-wrap">{ title }</p>
        {
          desc &&  <p className=" text-gray-500">{ desc }</p>
        }
      </span>
      {
        price && <p className="whitespace-no-wrap">{ price }</p>
      }
    </button>
  )
}

export default CustomButton