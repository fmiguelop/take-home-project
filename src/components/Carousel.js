import { Component } from "react";
import Rear from '../../public/img/kemper-rear.jpg';
import Angle from '../../public/img/kemper-angle.jpg'
import Front from '../../public/img/kemper-front.jpg'
import Image from 'next/image';


const images = [{ name:'Front', img: Front, sel: true, }, { name:'Angle', img: Angle, sel: false }, { name:'Rear', img: Rear, sel: false }];

class Carousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 0
    };
  }

  handleIndexClick = (event) => {
    this.setState({
      active: +event.target.dataset.index,
    })

    images.forEach((img, i) => {
      if (i !== +event.target.dataset.index) img.sel = false
      else img.sel = true
    })
  }

  render() {
    const { active } = this.state;
    return (
      <div className="pt-2 lg:sticky inset-0 lg:mb-40">
        <div className="shadow-sm rounded-md border-2 border-gray-300 mt-8">
          <Image
            src={images[active].img} alt="Kemper"
            className="rounded-md"/>
        </div>
        <div className="flex justify-between items-center mt-4">
          {images.map((image, i) => (
            <button
              key={image.name}
              className={`${image.sel ? 'focus:shadow-outline-gray  focus:border-gray-700 shadow-outline-gray  border-gray-700' : 'shadow-sm border-gray-200'} rounded-lg mx-3 border hover:border-gray-400 focus:outline-none focus:shadow-outline-gray  focus:border-gray-700 active:shadow-outline-gray`}>
              <Image
                className="rounded-lg"
                src={image.img}
                data-index={i}
                onClick={this.handleIndexClick}
                alt={`Kemper ${image.img}`}
              />
            </button>
          ))}
        </div>
      </div>
    )
  }
}

export default Carousel;