
import Image from 'next/image'
import PropTypes from 'prop-types';
import Globe from '../../public/img/globe-icon.svg'
import Shield from '../../public/img/shield-check-icon.svg'

const Footer = ({ price }) => {
  Footer.PropTypes = {
    price: PropTypes.number
  }

  const numberFormat = (value) =>
    new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD'
    }).format(value);

  return (
    <div className="bg-gray-50 fixed inset-x-0 bottom-0 border-t-2 border-gray-200 flex justify-end md:justify-around lg:justify-between items-center">
      <span className="hidden md:flex md:flex-col lg:grid lg:grid-cols-2 lg:mx-8 lg:py-4">
        <span className="">
          <span className="inline-flex">
            <Image src={Globe} />
            <p className="ml-4"> Free Shipping</p>
          </span>
          <p className="hidden lg:block ml-8 text-sm font-normal leading-5 w-64">Get 2-day free shipping anywhere in North America.</p>
        </span>
        <span>
          <span className="inline-flex">
            <Image src={Shield} />
            <p className="ml-4">2 Year Warranty</p>
          </span>
          <p className="hidden lg:block ml-8 text-sm font-normal leading-5 w-64">If anything goes wrong in the first two years, we'll replace it for free.</p>
        </span>
      </span>
      <span className="flex py-4 md:py-0 items-center">
        <span className="flex flex-col items-end">
          <p className="text-4xl font-extrabold text-gray-900">{numberFormat(price)}</p>
          <p className="hidden md:block text-sm font-normal leading-5 text-right whitespace-no-wrap">Need a financing? <button className='underline hover:no-underline hover:text-gray-700'>Learn more </button></p>
        </span>
        <button className="text-white bg-gray-900 rounded-md w-24 h-9 mx-6 hover:bg-gray-700 focus:outline-none focus:shadow-outline-gray  focus:border-gray-700">
          Buy Now
        </button>
      </span>
    </div>
  )
}

export default Footer
