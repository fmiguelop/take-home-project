import Head from 'next/head'
import { useState } from 'react'
import Footer from '../src/components/Footer'
import Carousel from '../src/components/Carousel'
import CustomButton from '../src/components/CustomButton'
import Specifications from '../src/components/Specifications'

const Home = () => {
  const [price, setPrice] = useState(1799)

  const [profilerBtn1, setProfilerBtn1] = useState(true)
  const [profilerBtn2, setProfilerBtn2] = useState(false)

  const [powerNoneBtn, setPowerNoneBtn] = useState(true)
  const [poweredBtn1, setPoweredBtn1] = useState(false)

  const [footNoneBtn, setFootNoneBtn] = useState(true)
  const [footRemoteBtn, setfootRemoteBtn] = useState(false)

  const changeToRack = () => {
    setProfilerBtn2(true)
    setProfilerBtn1(false)
  }

  const changeToHead = () => {
    setProfilerBtn2(false)
    setProfilerBtn1(true)
  }

  const changeToNoneAmp = () => {
    setPowerNoneBtn(true)
    setPoweredBtn1(false)
    const newPrice = price - 449;
    if (price === 1799) { setPrice(1799) }
    else if (price > 1799) { setPrice(newPrice) }
  }

  const changeToPoweredAmp = () => {
    setPoweredBtn1(true)
    setPowerNoneBtn(false)
    const newPrice = price + 449;
    if (price < 2697) { setPrice(newPrice) }
  }

  const changeToNoneFoot = () => {
    setFootNoneBtn(true)
    setfootRemoteBtn(false)
    const newPrice = price - 449;
    if (price === 1799) { setPrice(1799) }
    else if (price > 1799) { setPrice(newPrice) }
  }

  const changeToFootController = () => {
    setfootRemoteBtn(true)
    setFootNoneBtn(false)
    const newPrice = price + 449;
    if (price < 2697) { setPrice(newPrice) }
  }

  return (
    <div className="mx-4 md:mx-12">
      <Head>
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
      </Head>
      <div className=" mt-12 mb-4 font-sans">
        <h1 className="leading-10 text-4xl text-gray-900 font-extrabold whites">Get your Kemper Profiling Amp</h1>
        <h2 className="leading-7 text-xl text-gray-500 mt-4 md:mt-3 mb-4 md:mb-6">All your favorite amps and effects, together in one little box.</h2>
      </div>
      <hr className="h-px bg-gray-300" />
      <div className="lg:grid lg:grid-cols-2 lg:gap-10 md:mb-24 lg:mb-32">
        <div>
          <Carousel />
        </div>
        <div className="">
          <div className="mt-8">
            <p className="text-gray-500 text-sm">Starting at</p>
            <p className="text-4xl font-extrabold text-gray-900">$1,799.00</p>
          </div>
          <div className="mt-6">
            <p className="text-lg text-gray-900">The KEMPER PROFILER™ is the leading-edge digital guitar amplifier and all-in-one effects processor.</p>
            <p className="text-gray-500 my-4">Hailed as a game-changer by guitarists the world over, the PROFILER™ is the first digital guitar amp to really nail the full and dynamic sound of a guitar or bass amp.</p>
            <p className="text-gray-500">This is made possible by a radical, patented technology and concept which we call "PROFILING".</p>
          </div>
          <div className="mt-12">
            <h3 className="text-gray-900 font-medium text-lg leading-7">Form Factor</h3>
            <span className="inline-grid md:grid-cols-2 gap-4">
              <CustomButton
                title='Profiler Head'
                desc='Compact amplifier head, perfect for a speaker cabinet or desk.'
                selected={profilerBtn1}
                onClick={(e) => {
                  e.preventDefault()
                  changeToHead()
                }} />
              <CustomButton
                title='Profiler Rack'
                desc='3U rackmount version of the classic profiling amplifier.'
                selected={profilerBtn2}
                onClick={(e) => {
                  e.preventDefault()
                  changeToRack()
                }} />
            </span>
          </div>
          <div className="mt-12">
            <h3 className="text-gray-900 font-medium text-lg leading-7">Power Amp</h3>
            <CustomButton
              title='None'
              desc='Use in the studio or with your own power amp.'
              selected={powerNoneBtn}
              onClick={(e) => {
                e.preventDefault()
                changeToNoneAmp()
              }} />
            <CustomButton
              title='Powered'
              desc='Built-in 600W solid state power amp.'
              price='+ $449.00'
              selected={poweredBtn1}
              onClick={(e) => {
                e.preventDefault()
                changeToPoweredAmp()
              }} />
          </div>
          <div className="mt-12">
            <h3 className="text-gray-900 font-medium text-lg leading-7">Foot Controller</h3>
            <CustomButton
              title='None'
              selected={footNoneBtn}
              onClick={(e) => {
                e.preventDefault()
                changeToNoneFoot()
              }} />
            <CustomButton
              title='Profiler Remote Foot Controller  '
              price='+ $449.00'
              selected={footRemoteBtn}
              onClick={(e) => {
                e.preventDefault()
                changeToFootController()
              }} />
          </div>
          <h3 className="text-gray-900 font-medium text-lg leading-7 mt-12">Specifications</h3>
          <Specifications />
        </div>
      </div>
      <Footer price={price} />
    </div>
  )
}

export default Home
